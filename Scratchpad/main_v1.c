/**
 * V1_OLD main function
 * This version did everything on a single process and worked successfully!
 *
 **/

int main(int argc, const char * argv[])
{
    if ( !checkArgs( argc, argv ) )
    { // Check arguments and exit if not enough arguments!
        exit(EXIT_FAILURE);
    }
    
    // Matrix a is defined by the first argument, with dimensions M and N
    int **matrix_a = readFile( argv[1], atoi( argv[3] ), atoi( argv[4]) );
    
    // Matrix b is defined by the second argument, with dimensions N and K
    int **matrix_b = readFile( argv[2], atoi( argv[4] ), atoi( argv[5]) );
    
    int **matrix_c = matrixMultiply(
                                    matrix_a, matrix_b, atoi( argv[3] ),
                                    atoi( argv[4] ), atoi( argv[5] )
                                    );
    
    int total = (
                 calcSubTotal( matrix_c, 0, atoi( argv[5]) ) +
                 calcSubTotal( matrix_c, 1, atoi( argv[5]) ) +
                 calcSubTotal( matrix_c, 2, atoi( argv[5]) )
                 );
    
    fprintf( stderr, "Final total is: %d \n", total );
    
    printf("\nProgram Complete!\n");
    return 0;
}