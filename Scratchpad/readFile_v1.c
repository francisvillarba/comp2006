//
//  readFile_v1.c
//  COMP2006
//
//  Version 1 of readFile.c
//  This version is the initial version which was working for the /base/
//  implentation of reading a file into an array
//
//  Created by Francis Villarba on 8/05/2016.
//  Copyright © 2016 Francis Villarba. All rights reserved.
//

#include <stdio.h>

int** readFile( const char * fileName, int rows, int columns )
{
    FILE * nData;
    nData = fopen( fileName, "r" ); // Read only open
    
    if ( nData == NULL ) // If we couldn't open the file
    {
        perror("The following error has occured");
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf( stderr, "Reading file: %s \n", fileName );
        int **matrix = makeArray( rows, columns );
        
        // The values that we will need to hold onto for processing
        char *line = NULL;
        size_t len = 0;
        ssize_t read;
        
        int current_row = 0;
        int current_column = 0;
        
        
        // Read the file until there is no more lines to read
        while ( ( read = getline( &line, &len, nData) ) != EOF )
        {
            // Initial value
            char * currentValue = strtok( line, DELIMITER );
            
            while( currentValue != NULL )
            {
                matrix[ current_row ][ current_column ] = atoi( currentValue );
                // Debug statement -- with stderr for support with Mac command line
                fprintf(stderr, "Matrix[%i][%i]: %d \n", current_row, current_column, matrix[current_row][current_column] );
                currentValue = strtok( NULL, DELIMITER );
                current_column++;
            }
            current_column = 0;
            current_row++;
        }
        fprintf(stderr, "Finished reading file: %s \n\n", fileName );
        return matrix;
    }
}