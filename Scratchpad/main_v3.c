//
//  main_v3.c
//  COMP2006
//
//  Created by Francis Villarba on 8/05/2016.
//  Copyright © 2016 Francis Villarba. All rights reserved.
//

#include <stdio.h>

/* General housekeeping stuff in main                                     */

if ( !checkArgs( argc, argv ) )
{
    exit( EXIT_FAILURE );
}

/* Important stuff before we can handle shares                            */

signal( SIGINT, signal_callback_handler ); // Register handle for CTRL+C
initStructs( argv );
int sizeOfSubtotalStruct = sizeof( subtotal );
int sizeOfMatrixStruct = (
                          sizeof( matrices.matrix_a ) +
                          sizeof( matrices.matrix_b ) +
                          sizeof( matrices.matrix_c )
                          );

/* For matrix shares                                                      */

int shm_fd_matrix;                      // Descriptor from shm_open function
char *shm_base_matrix;                  // The base address from mmap() func

shm_fd_matrix = shm_open(
                         matrixShareName,
                         O_CREAT | O_RDWR,
                         S_IRUSR | S_IWUSR
                         );

if ( shm_fd_matrix < 0 )        // If we can't make memory share, error out!
{
    perror( "shmat" );
    exit( EXIT_FAILURE );
}

ftruncate( shm_fd_matrix, 1 * sizeOfMatrixStruct ); // Init memory for share

// Map it so we can use it
shm_base_matrix = mmap(
                       0,
                       sizeOfMatrixStruct,
                       PROT_READ | PROT_WRITE,
                       MAP_SHARED,
                       shm_fd_matrix,
                       0
                       );

// What to do if we cannot map
if ( shm_base_matrix == MAP_FAILED )
{
    fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
    exit( EXIT_FAILURE );
}

/* For subtotal shares                                                    */

int shm_fd_subtotal;
char *shm_base_subtotal;

shm_fd_subtotal = shm_open(
                           subShareName,
                           O_CREAT | O_RDWR,
                           S_IRUSR | S_IWUSR
                           );

if ( shm_fd_subtotal < 0 )
{
    perror( "shmat" );
    exit( EXIT_FAILURE );
}

ftruncate( shm_fd_subtotal, 1 * sizeOfSubtotalStruct );

shm_base_subtotal = mmap(
                         0,
                         sizeOfSubtotalStruct,
                         PROT_READ | PROT_WRITE,
                         MAP_SHARED,
                         shm_fd_subtotal,
                         0
                         );

if ( shm_base_subtotal == MAP_FAILED )
{
    fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
    exit( EXIT_FAILURE );
}

/* Read the files                                                         */

readFile( argv[2], atoi( argv[4] ), atoi( argv[5]), 'a', sizeOfMatrixStruct, shm_fd_matrix );

exit( EXIT_SUCCESS );