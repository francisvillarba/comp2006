//
//  main_v2.c
//  COMP2006
//
//  This is the second version of the main function - this time, shared memory
//  was partially integrated!
//
//  Created by Francis Villarba on 8/05/2016.
//  Copyright © 2016 Francis Villarba. All rights reserved.
//

#include <stdio.h>

int main( int argc, const char *argv[] )
{
    // Check arguments and exit if not enough arguments!
    if ( !checkArgs( argc, argv ) )
    {
        exit( EXIT_FAILURE );
    }
    
    // Register the event handler for CTRL+C
    signal( SIGINT, signal_callback_handler );
    
    int shm_fd;                           // Descriptor from shm_open() function
    char *shm_base;                       // The base address from mmap() func
    
    // Create the shared memory block
    shm_fd = shm_open( semName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR );
    // Create a read write object, that a user can read and write into
    
    // If we can't make the shm_fd, explode! (not really, exit instead)
    if( shm_fd < 0 )
    {
        perror("shmat");
        exit( EXIT_FAILURE );
    }
    
    // Debug prompt -- TODO -- Remove this on final version
    fprintf( stderr, "Shared memory object created for program: %s \n", semName );
    
    // Now we will allocate memory to the shared block for use
    ftruncate( shm_fd, 1 * sizeof( subtotal ) );
    
    // Map shared memory segment to the address space of the current process
    shm_base = mmap(
                    0, 1 * sizeof( subtotal ),
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED, shm_fd, 0
                    );
    
    // Check if the map was successful or not
    if ( shm_base == MAP_FAILED )
    {
        fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    // Debug prompt -- TODO -- Remove this on final version
    fprintf( stderr, "Shared memory segment was allocated successfully! \n" );
    
    exit( EXIT_SUCCESS );
}