/******************************************************************************/
/*                                                                            */
/*  Header File Name: main.c                                                  */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 1                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 27th April 2016                                                     */
/*                                                                            */
/*  Description: Runs the program with parameters file1 file2 M N K           */
/*      where M is the number of rows in file 1, N is number of columns in    */
/*      file 1 (and rows in file 2) and K is the number of columns in file 2  */
/*                                                                            */
/******************************************************************************/

#ifndef main_h
#define main_h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "readfile.h"
#include "calculation.h"
#include "shared.h"

const char *shareMatrixA = "/pmms-matrix-a";
const char *shareMatrixB = "/pmms-matrix-b";
const char *shareMatrixC = "/pmms-matrix-b";
const char *share = "/pmms-stShare";

sem_t *mutex;

#endif /* main_h */
