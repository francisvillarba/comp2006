//
//  calculation.h
//  COMP2006
//
//  Created by Francis Villarba on 5/05/2016.
//  Copyright © 2016 Francis Villarba. All rights reserved.
//

#ifndef calculation_h
#define calculation_h

#include <stdio.h>

#include "matrix.h"
#include "shared.h"

void matrixMultiply( int shm_mat_a, int shm_mat_b, int shm_mat_c, int m, int n, int k );
int calcSubTotal( int** matrixC, int row, int columns );
void calculationDispatcher( int shm_mat_a, int shm_mat_b, int shm_mat_c, int m, int n, int k );

#endif /* calculation_h */
