/******************************************************************************/
/*                                                                            */
/*  Source File Name: readFile.c                                              */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 2                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 27th April 2016                                                     */
/*  Description: Reads the file into an array                                 */
/*                                                                            */
/******************************************************************************/

#include "readFile.h"

/** Current implementation  ***************************************************/

void readMatrix( const char* fileName, int rows, int columns, int shm_id, int size )
{
    /** Initial steps required for this                                       */
    
    // Open the file itself
    FILE * nData = fopen( fileName, "r" );
    
    if ( nData == NULL ) // If we couldn't open the file
    {
        perror("The following error has occured");
        exit(EXIT_FAILURE);
    }
    
    // Open the shared memory
    char *shm_base_matrix_generic = mmap(
                                         0,
                                         size,
                                         PROT_READ | PROT_WRITE,
                                         MAP_SHARED,
                                         shm_id,
                                         0
                                         );
    
    if( shm_base_matrix_generic == MAP_FAILED )
    {
        fprintf( stderr, "pmms -- map failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    /** Standard Read File Procedure                                          */
    
    fprintf( stderr, "Reading file: %s \n", fileName );
    int **matrix = makeArray( rows, columns );
    
    // The values that we will need to hold onto for processing
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    
    int current_row = 0;
    int current_column = 0;
    
    
    // Read the file until there is no more lines to read
    while ( ( read = getline( &line, &len, nData) ) != EOF )
    {
        // Initial value
        char * currentValue = strtok( line, DELIMITER );
        
        while( currentValue != NULL )
        {
            matrix[ current_row ][ current_column ] = atoi( currentValue );
            // Debug statement -- with stderr for support with Mac command line
            fprintf(stderr, "Matrix[%i][%i]: %d \n", current_row, current_column, matrix[current_row][current_column] );
            currentValue = strtok( NULL, DELIMITER );
            current_column++;
        }
        current_column = 0;
        current_row++;
    }
    fprintf(stderr, "Finished reading file: %s \n\n", fileName );
    
    /** Now we put all this data into our shared memory block                 */
    memcpy( shm_base_matrix_generic, matrix, size );
    
    free( matrix );
    
    /** Close the files!                                                      */
    fclose( nData );
}

/** Older implementation                                                      */

/******************************************************************************/
/*                                                                            */
/* Reuse instructions:                                                        */
/*  int** readFile( const char *fileName, int rows, int columns)              */
/*  PURPOSE: Reads the following file into an array then gives it back        */
/*  LIMITATIONS: Requires you to know the rows and columns, does not check    */
/*               if it is going out of bounds (again assuming your inputs     */
/*               are all correct                                              */
/*                                                                            */
/******************************************************************************/

int** readFile( const char * fileName, int rows, int columns )
{
    FILE * nData;
    nData = fopen( fileName, "r" ); // Read only open
    
    if ( nData == NULL ) // If we couldn't open the file
    {
        perror("The following error has occured");
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf( stderr, "Reading file: %s \n", fileName );
        int **matrix = makeArray( rows, columns );
        
        // The values that we will need to hold onto for processing
        char *line = NULL;
        size_t len = 0;
        ssize_t read;
        
        int current_row = 0;
        int current_column = 0;
        
        
        // Read the file until there is no more lines to read
        while ( ( read = getline( &line, &len, nData) ) != EOF )
        {
            // Initial value
            char * currentValue = strtok( line, DELIMITER );
            
            while( currentValue != NULL )
            {
                matrix[ current_row ][ current_column ] = atoi( currentValue );
                // Debug statement -- with stderr for support with Mac command line
                fprintf(stderr, "Matrix[%i][%i]: %d \n", current_row, current_column, matrix[current_row][current_column] );
                currentValue = strtok( NULL, DELIMITER );
                current_column++;
            }
            current_column = 0;
            current_row++;
        }
        fprintf(stderr, "Finished reading file: %s \n\n", fileName );
        return matrix;
    }
}