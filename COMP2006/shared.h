/******************************************************************************/
/*                                                                            */
/*  Header File Name: shared.h                                                */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 1st May 2016                                                        */
/*                                                                            */
/*  Description: Header to deal with memory sharing                           */
/*                                                                            */
/******************************************************************************/

#ifndef shared_h
#define shared_h

// General Imports
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// For memory sharing
#include <semaphore.h> /* Needed to create semaphore to stop race condition */
#include <sys/mman.h> /* Needed for memory mapping */
#include <sys/stat.h> /* Needed for S_* mode constants*/
#include <fcntl.h> /* Needed for O_* constants */
#include <unistd.h> /* Needed for ftruncate function */

// For memcpy function
#include <string.h>

// For support for Mac OS X forks
#include <sys/types.h>
#include <sys/param.h>

// Used for clean close on signal
#include <signal.h>

#endif /* shared_h */