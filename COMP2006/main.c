/******************************************************************************/
/*                                                                            */
/*  Source File Name: main.c                                                  */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 1                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 27th April 2016                                                     */
/*                                                                            */
/*  Description: Runs the program with parameters file1 file2 M N K           */
/*      where M is the number of rows in file 1, N is number of columns in    */
/*      file 1 (and rows in file 2) and K is the number of columns in file 2  */
/*                                                                            */
/******************************************************************************/

#include "main.h"

/** Error Check Procedures ****************************************************/

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  bool checkArgs( int argc, const char * argv[] )                           */
/*  PURPOSE: Checks the arguments (in a modular fashion)                      */
/*  LIMITATIONS: Needs stdlib.h and stdbool.h, only checks if correct number  */
/*               of arguments have been supplied and of the right type        */
/*  RETURN: true if valid, false otherwise                                    */
/******************************************************************************/

bool checkArgs( int argc, const char * argv[] )
{
    bool result = false;
    if ( argc != 6 )
    {
        printf( "Not enough arguments given, expected 6, given %d", argc );
        result = false;
    }
    else
    {
        result = true;
    }
    return result;
}

/** Signal callback code ******************************************************/

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  void signal_callback_handler( int signum )                                */
/*  PURPOSE: Ensure we close the program cleanly after reciving a signal      */
/*           interrupt at somepoint during runtime (such as CTRL^C on Term    */
/*  LIMITATIONS: requires a valid signal and assumes we are using semaphores  */
/******************************************************************************/

void signal_callback_handler( int signum )
{
    if ( shm_unlink( shareMatrixA ) != 0 )
    {
        perror( "In shm_unlink() function - Line 58" );
        exit( EXIT_FAILURE );
    }
    
    if ( shm_unlink( shareMatrixB ) != 0 )
    {
        perror( "In shm_unlink() function - Line 64" );
        exit( EXIT_FAILURE );
    }
    
    if ( shm_unlink( shareMatrixC ) != 0 )
    {
        perror( "In shm_unlink() function - Line 70" );
        exit( EXIT_FAILURE );
    }
    
    exit( signum ); // Exit the system based on the signal number given
}

/** Main processing occurs here  **********************************************/

int main( int argc, const char *argv[] )
{
    /* General housekeeping stuff in main                                     */
    if ( !checkArgs( argc, argv ) )
    {
        exit( EXIT_FAILURE );
    }
    
    int m = atoi( argv[3] );                                      // Dimension M
    int n = atoi( argv[4] );                                      // Dimension N
    int k = atoi( argv[5] );                                      // Dimension K
    
    /* Shared Main Portions                                                   */
    
    signal( SIGINT, signal_callback_handler ); // Register handle for CTRL+C
    
    
    // Descriptors for shared matrix memories
    int shm_fd_matrix_a;
    int shm_fd_matrix_b;
    int shm_fd_matrix_c;
    
    // The base address from mmap() function for matrices
    char *shm_base_matrix_a;
    char *shm_base_matrix_b;
    char *shm_base_matrix_c;
    
    // Subtotal versions
    int shm_fd_subtotal;
    char *shm_base_subtotal;
    
    /* Create the shared memories and check them on the spot                  */
    
    shm_fd_matrix_a = shm_open(                                      // Matrix_A
                               shareMatrixA,
                               O_CREAT | O_RDWR,
                               S_IRUSR | S_IWUSR
                               );
    // If we could not allocate a share
    if( shm_fd_matrix_a < 0 )
    {
        perror( "shmat" );
        exit( EXIT_FAILURE );
    }
    // Now allocate memory to this shared space
    ftruncate( shm_fd_matrix_a, 1 * m * n );
    // Now we map it so we can try using it
    shm_base_matrix_a = mmap(
                             0,
                             1 * m * n,
                             PROT_READ | PROT_WRITE,
                             MAP_SHARED,
                             shm_fd_matrix_a,
                             0
                             );
    // If we could not map this memory, fail LOUDLY!
    if( shm_base_matrix_a == MAP_FAILED )
    {
        // Use frpintf for mac compatibility -- especially with IDEs like Xcode
        fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    shm_fd_matrix_b = shm_open(                                      // Matrix_B
                               shareMatrixB,
                               O_CREAT | O_RDWR,
                               S_IRUSR | S_IWUSR
                               );
    
    if( shm_fd_matrix_b < 0 )
    {
        perror( "shmat" );
        exit( EXIT_FAILURE );
    }
    
    ftruncate( shm_fd_matrix_b, 1 * n * k );
    
    shm_base_matrix_b = mmap(
                             0,
                             1 * n * k,
                             PROT_READ | PROT_WRITE,
                             MAP_SHARED,
                             shm_fd_matrix_b,
                             0
                             );
    
    if( shm_base_matrix_b == MAP_FAILED )
    {
        fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    shm_fd_matrix_c = shm_open(                                      // Matrix_C
                               shareMatrixC,
                               O_CREAT | O_RDWR,
                               S_IRUSR | S_IWUSR
                               );
    
    if( shm_fd_matrix_c < 0 )
    {
        perror( "shmat" );
        exit( EXIT_FAILURE );
    }
    
    ftruncate( shm_fd_matrix_c, 1 * m * k );
    
    shm_base_matrix_c = mmap(
                             0,
                             1 * m * k,
                             PROT_READ | PROT_WRITE,
                             MAP_SHARED,
                             shm_fd_matrix_c,
                             0
                             );
    
    if( shm_base_matrix_c == MAP_FAILED )
    {
        fprintf( stderr, "pmms - mmap failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    shm_fd_subtotal = shm_open(                                      // Subtotal
                               share,
                               O_CREAT | O_RDWR,
                               S_IRUSR | S_IWUSR
                               );
    
    if( shm_fd_subtotal < 0 )
    {
        perror( "shmat" );
        exit( EXIT_FAILURE );
    }
    
    ftruncate( shm_fd_subtotal, 1 * sizeof( int ) );
    
    shm_base_subtotal = mmap(
                             0,
                             1 * sizeof( int ),
                             PROT_READ | PROT_WRITE,
                             MAP_SHARED,
                             shm_fd_subtotal,
                             0
                             );
    
    if( shm_base_subtotal == MAP_FAILED )
    {
        fprintf( stderr, "pmms -- map failed with status: MAP_FAILED" );
        exit( EXIT_FAILURE );
    }
    
    /** Read the file into arrays for processing                              */
    
    readMatrix( argv[1], m, n, shm_fd_matrix_a, 1 * m * n );
    readMatrix( argv[2], n, k, shm_fd_matrix_b, 1 * n * k );
    
    /** Matrix Calculations finally begin!                                    */
    calculationDispatcher( shm_fd_matrix_a, shm_fd_matrix_b, shm_fd_matrix_c, m, n, k );
    
    exit( EXIT_SUCCESS );
}