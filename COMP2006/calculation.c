/******************************************************************************/
/*                                                                            */
/*  Source File Name: calculation.c                                           */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 4                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 29th April 2016                                                     */
/*  Description: Runs the matrix calculations for a given value               */
/*                                                                            */
/******************************************************************************/

#include "calculation.h"

/** Signal Handlers ***********************************************************/

void signal_process_handler( int signum )
{
    
}

/** Inner implemenation for matrix multiplication *****************************/

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  int calcSingleResult( int rowA[], int columnB[], int n )                  */
/*  PURPOSE: Calculates the result of a single portion of C's matrix where    */
/*           rowA[] defines the array of numbers in the row of A, columnB[]   */
/*           defines the array of numbers in the column of B, int n is the    */
/*           number of columns in A (and number of rows in B)                 */
/*  RETURN: Resulting matrix after matrix multiplication                      */
/******************************************************************************/

int calcSingleResult( int rowA[], int columnB[], int n )
{
    int result = 0;
    for( int i = 0; i < n; i ++ )
    {
        result = result + ( rowA[i] * columnB[i] );
    }
    return result;
}

/** Exposed calculation for matrix multiplication *****************************/

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  int** matrixMultiply( int** matrixA, int** matrixB, int m, int n, int k ) */
/*  PURPOSE: Calculates the resulting matrix when we multiply each value      */
/*  LIMITATIONS: Requires m, n and k which describe the dimensions of matrix  */
/*  RETURN: Resulting matrix after matrix multiplication                      */
/******************************************************************************/

void matrixMultiply( int shm_mat_a, int shm_mat_b, int shm_mat_c, int m, int n, int k )
{
    /* Map the shared memory                                                  */
    
    char *shm_base_matrix_a = mmap(                                  // Matrix_A
                                   0,
                                   1 * m * n,
                                   PROT_READ | PROT_WRITE,
                                   MAP_SHARED,
                                   shm_mat_a,
                                   0
                                   );
    
    char *shm_base_matrix_b = mmap(                                  // Matrix_B
                                   0,
                                   1 * n * k,
                                   PROT_READ | PROT_WRITE,
                                   MAP_SHARED,
                                   shm_mat_b,
                                   0
                                   );
    
    char *shm_base_matrix_c = mmap(                                  // Matrix_C
                                   0,
                                   1 * m * k,
                                   PROT_READ | PROT_WRITE,
                                   MAP_SHARED,
                                   shm_mat_c,
                                   0
                                   );
    
    // TODO - Check if the map was successful!
    
    /* Map the memory of the function                                         */
    
    int** matrixA;
    int** matrixB;
    
    memcpy( matrixA, shm_base_matrix_a, 1 * m * n);
    memcpy( matrixB, shm_base_matrix_b, 1 * n * k);
    
    int** result = makeArray( m, k );
    
    fprintf( stderr, "Calculating Matrix C \n" );
    for( int rows = 0; rows < m; rows++ )
    {
        for( int columns = 0; columns < k; columns++ )
        {
            int rowA[ n ]; // Make an array the size of num of cols in A
            int colB[ n ]; // Make an array thne size of num of rows in B
            
            for( int i = 0; i < n; i++ )
            {
                rowA[i] = matrixA[rows][i];
                colB[i] = matrixB[i][columns];
            }
            
            result[rows][columns] = calcSingleResult( rowA, colB, n );
            fprintf( stderr, "Matrix[%d][%d]: %d \n", rows, columns, result[rows][columns] );
        }
    }
    fprintf( stderr, "Finished calculation of Matrix C \n\n" );
    
    free( matrixA );
    free( matrixB );
}

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  int calcSubTotal( int** matrixC, int row, int columns )                   */
/*  PURPOSE: Calculates the subtotal of a given row in MatrixC                */
/*  LIMITATIONS: Requires matrixC, the row to look at and the # of columns c  */
/*  RETURN: The result of the calculation as an integer                       */
/******************************************************************************/

int calcSubTotal( int** matrixC, int row, int columns )
{
    int result = 0;
    
    for( int i = 0; i < columns; i++ )
    {
        result = result + matrixC[row][i];
    }
    fprintf( stderr, "DEBUG: Result of subtotal %d: %d \n", row, result );
    
    return result;
}

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  void calculationDispatcher( int shm_mat_a, int shm_mat_b,                 */
/*                              int shm_mat_c, int m, int n, int k )          */
/*  PURPOSE: Calculates the subtotal of a given row in MatrixC                */
/*  LIMITATIONS: Requires matrixC, the row to look at and the # of columns c  */
/*  RETURN: The result of the calculation as an integer                       */
/******************************************************************************/
void calculationDispatcher( int shm_mat_a, int shm_mat_b, int shm_mat_c, int m, int n, int k )
{
    pid_t pid;
    
    pid = fork();
    
    // If we cannot make a child, explosions shall occur!
    if( pid < 0 )
    {
        fprintf( stderr, "Unable to create child" );
        exit( EXIT_FAILURE );
    }
    else if ( pid == 0 )                          // Children is always pid == 0
    {
        matrixMultiply( shm_mat_a, shm_mat_b, shm_mat_c, m, n, k);
    }
    else                                                 // The parent does this
    {
        wait(NULL);
    }
}