/******************************************************************************/
/*                                                                            */
/*  Header File Name: readFile.c                                              */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 2                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 27th April 2016                                                     */
/*  Description: Reads the file into an array                                 */
/*                                                                            */
/******************************************************************************/


#ifndef readFile_h
#define readFile_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "shared.h"

#define DELIMITER " "

int** makeArray( int rows, int columns );
void freeArray( int** array );

void readMatrix( const char* fileName, int rows, int columns, int shm_id, int size );

int** readFile( const char * fileName, int rows, int columns );

#endif /* readFile_h */