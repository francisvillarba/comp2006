//
//  matrix.h
//  COMP2006
//
//  Created by Francis Villarba on 5/05/2016.
//  Copyright © 2016 Francis Villarba. All rights reserved.
//

#ifndef matrix_h
#define matrix_h

#include <stdio.h>
#include <stdlib.h>

int** makeArray( int rows, int columns );
void freeArray( int** array );

#endif /* matrix_h */
