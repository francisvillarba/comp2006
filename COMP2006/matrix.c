/******************************************************************************/
/*                                                                            */
/*  Source File Name: matrix.c                                                */
/*  Subject: COMP2006 - Operating Systems                                     */
/*  Program Assignment: 5                                                     */
/*  Name: Francis Czedrick Villarba                                           */
/*  Student Number: 18266997                                                  */
/*  Date: 27th April 2016                                                     */
/*  Description:  Deals with the creation and destruction of matrices         */
/*                                                                            */
/******************************************************************************/

#include "matrix.h"

/** Array Implementation ******************************************************/

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  int** makeArray( int rows, int columns)                                   */
/*  PURPOSE: Creats an array based on row and column sizes (given as int) and */
/*           initialises all the values to zero                               */
/*  LIMITATIONS: None that i am aware of                                      */
/*  RETURN: Returns a pointer to a pointer of the array                       */
/******************************************************************************/

int** makeArray( int rows, int columns )
{
    int **matrix = calloc( rows, sizeof( *matrix ) );
    int *data   = calloc( columns * rows, sizeof( *data ) );
    
    for (int i = 0; i < rows; i++)
    {
        matrix[i] = &data[i * columns];
    }
    
    return matrix;
}

/******************************************************************************/
/* Reuse instructions:                                                        */
/*  void freeArray( int** array )                                             */
/*  PURPOSE: Frees the memory allocated to the specified array                */
/*  LIMITATIONS: Assumes that you are giving it an array you have created via */
/*                  the int** makeArray function and that the array is not    */
/*                  null                                                      */
/******************************************************************************/

void freeArray( int** array )
{
    free( *array );          // Free the array pointer
    free( array );           // Free the array itself
}
